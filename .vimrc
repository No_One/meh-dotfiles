let mapleader = " "
syntax on
set number
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set textwidth=80
noremap - ddp
noremap _ ddkp
nnoremap <c-u> vbU
noremap <leader>b %
inoremap < <><esc>i
inoremap { {}<esc>i
inoremap [ []<esc>i
nnoremap <space><space> /<++><return>ves
noremap <leader>l $
